import re, time, json, os
import boto3                                                                                                # A low-level aws client
from datetime import timedelta,datetime

# Regular Expressions for log parsing 
date_regex = re.compile(r"^\d+-\d+-\d+T[\w:.]+")                                                            # parses timestamp at the begining of a line
secs_regex = re.compile(r"[\d]+[\.]+[\d]+(?=\ssecs)")                                                       # parses seconds from log
gc_full_regex = re.compile(r"Full\sGC\s")                                                                   # parses Full GC event from log
pre_gc_mem_regex = re.compile(r"\s[\d]+(?=M)")                                                              # parses memory utilization before GC event
post_gc_mem_regex = re.compile(r"(?<=(->))[\d]+(?=M)")                                                      # parses memory utilization after GC event

current_datetime = datetime.strptime("2017-03-01T15:45:00.551","%Y-%m-%dT%H:%M:%S.%f")

# Parses from lists of events that needs to be logged to NewRelic 
newrelic_logs_to_match = ["Full\sGC\s","GC\spause\s","GC\sconcurrent-root-region-scan-end","GC\sconcurrent-mark-end","GC\sconcurrent-cleanup-end","GC\scleanup","GC\sremark"]

#define dictionary to store last five min stat
gc_full_space_history = []                                                                                  # list to track gc full events in last 5 mins
gc_full_time_history = {}                                                                                   # dictionary to track gc full total time in last 5 mins
newrelic_json_arr = []                                                                                      # JSON array for events that needs to be pushed to NewRelic 

# Environment variable declaration
log_file = 'logs/gc.log'
elb_name = 'java_app_ec2_cluster_elb'
newrelic_insight_api_url = 'https://insights-collector.newrelic.com/v1/accounts/2398071/events'


def main():                                                                                                 # function: main 
    for line in read_log_trail(log_file):                                                                   # continuously reads latest line from log file
        print "New log line fetched"
        current_datetime = getDate("2017-03-01T15:44:15.656")                                               # predefined timestamp for exisitng log file in repo
        #current_datetime = datetime.strptime(datetime.utcnow(),"%Y-%m-%dT%H:%M:%S.%f")                     # current timestamp for live logs
        for newrelic_regex in newrelic_logs_to_match:                                                       # traverses NewRelic regex list
            if re.compile(newrelic_regex).search(line):                                                              
                addMetricsToNewRelicJsonArray(line, newrelic_regex)                                         # log line needs to be sent to NewRelic
                
                if gc_full_regex.search(line):                                                              # check if event is GC Full
                    line_datetime = getDate(date_regex.search(line).group())                                # parse timestamp from log line

                    #if withinFiveMin(current_datetime, line_datetime):                                     # check if timestamp within 
                    exec_sec = float(secs_regex.search(line).group(0).strip())                              # get total seconds spent on GC Full event
                    updateGCFullTimeHistory(line_datetime, exec_sec)                                        # update seconds to five minute history list and check if restart is required

                    pre_gc_mem = float(pre_gc_mem_regex.search(line).group())                               # get memory utilization before GC Full event
                    post_gc_mem = float(post_gc_mem_regex.search(line).group())                             # get memory utilization after GC Full event
                    if ((100-(post_gc_mem/pre_gc_mem*100))<20):                                             # check if total memory freed is less than 20%
                        updateGCFullFreeSpaceHistory(line_datetime)                                         # add event to five minute history list and check if restart is required


def read_log_trail(log_file):                                                                               # function: continuously checks if new logs is present and returns log line
    current = open(log_file, "r")                                                                           # open file in readonly mode
    current_ino = os.fstat(current.fileno()).st_ino                                                         # fetch serial number of current log file
    while True:
        while True:
            line = current.readline()                                                                       # reads logs from file and keeps waiting for latest log line 
            if not line:
                break
            yield line                                                                                      # if new line is logged pass-on it to the main function

        try:
            if os.stat(log_file).st_ino != current_ino:                                                     # check if current log file is same or changed via log rotate by using file serial id
                new = open(log_file, "r")                                                                   # if files has rotated, reopen the file
                current.close()                                                                             # close the current file afterwards so that no logs are lost
                current = new                                                                               
                current_ino = os.fstat(current.fileno()).st_ino                                             # switch to the new log file
                continue
        except IOError:
            pass                                                                                            
        time.sleep(1)


def getDate(line_date_str):                                                                                 # function: parse date from log line
    print "function - getDate"
    return datetime.strptime(line_date_str,"%Y-%m-%dT%H:%M:%S.%f")


def withinFiveMin(current_datetime, line_datetime):                                                         # function: check if give timestamp is within five min of crrent timestamp
    time_diff = int((current_datetime - line_datetime).total_seconds())
    if (time_diff <= 300 and time_diff > 0):                                                                # if time diff is less than 5 min
        return True
    else:
        return False


def updateGCFullFreeSpaceHistory(line_datetime):                                                            # function: tracks occurances memory freed within five min is less than 20% 
    print "function - updateGCFullFreeSpaceHistory"
    for datetime in gc_full_space_history:                                                                  # iterate through timestamp from list
        if (withinFiveMin(current_datetime, datetime) == False):                                            # check if timestamp is within 5 min
            gc_full_space_history.remove(datetime)                                                          # remove timestamp from list as it's older than 5 min
            print "old timestamp removed"
    gc_full_space_history.append(line_datetime)                                                             # add new latest occurance timestamp to list
    
    if len(gc_full_space_history) >= 10:                                                                    # check if less than 20% memory freed within last five min occured 10 times 
        if checkELBInstanceHealth() == True:                                                                # check if other nodes instances in ELB are up 
            print "restarting tomcat"       
            restartTomcat()                                                                                 # call restart tomcat function


def updateGCFullTimeHistory(line_datetime, time):                                                           # function: tracks total time spent on GC Full event within last 5 min
    print "function - updateGCFullTimeHistory"
    gc_full_time_history.update({line_datetime:time})                                                       # Add timestamp and seconds spent on GC Full event
    total_full_gc_time = 0                                                                                  
    for datetime, secs in gc_full_time_history.items():                                                     # iterate through key-value map
        if (withinFiveMin(current_datetime, datetime) == False):                                            # check if timestamp is withing 5 min
            del gc_full_time_history[datetime]                                                              # remove timestamp and value as they are not within 5 min
            print "old timestamp removed"
        else:   
            total_full_gc_time += secs                                                                      # find total time spend on GC Full event in last 5 min
    if total_full_gc_time > 45:                                                                             # if total sime spent is 45 seconds (15% of 5 min)
        if checkELBInstanceHealth == True:                                                                  # check if other nodes instances in ELB are up
            print "restarting tomcat"                                                                   
            restartTomcat()                                                                                 # call restart tomcat function


def checkELBInstanceHealth():                                                                               # function: checks nodes status for ELB
    print "function - checkELBInstanceHealth"
    #response = boto3.client("elb").describe_instance_health(LoadBalancerName=elb_name)                     # get health status of all nodes in ELB
    #for instance in response['InstanceStates']:                                                            # iterate through all instances
    #     if instance['State'] != 'InService':                                                              # check if node is InService status
    #        return False                                                                                       
    return True


def restartTomcat():                                                                                        # function: restart tomcat
    print "function - restartTomcat"
    #os.system("sudo service tomcat restart")                                                               # restart tomcat systemd service on same machine
    print "Tomcat has been restarted"
    gc_full_space_history = []                                                                              # cleanup GC Full less than 20% freed space occurance list of 5 min history
    gc_full_time_history = {}                                                                               # cleanup GC Full total time spent less then 15% list of 5 min history 
        
def addMetricsToNewRelicJsonArray(line, regex):                                                             # function: add log line to json array for NewRelic Insights API
    print "function - addMetricsToNewRelicJsonArray"
    event_json = {}
    event_json['Timestamp'] = str(getDate(date_regex.search(line).group()))                                 # adds log line timestamp
    event_json['Event'] = re.compile(regex).search(line).group()                                            # adds event name i.e. GC Full, GC Pause etc.
    event_json['Stacktrace'] = line                                                                         # adds log line for stacktrace
    event_json['Duration'] = float(secs_regex.search(line).group(0).strip())                                # adds GC event duration
    newrelic_json_arr.append(event_json)                                                                    # adds json to json array that needs to be pushed to NewRelic Insight API
    print newrelic_json_arr     
    print ""

if __name__ == '__main__':                                                                                  # function: entry point
    main()

