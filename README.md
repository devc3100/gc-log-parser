# Garbage Collector Log Parser

Garbage collection log parser for Java Application

## Scripting Language

Python 2.7.16


## Assumptions

1. Script would be running on same machine as log file
2. EC2 Instance assumes role that allows it to query ELB healthcheck for other nodes


## Possible Improvements

Below are the possible improvement that could not have been completed due to time constraint:

1. Use object oriented script
2. Better exception handing
3. Date formatting using UTC and epoch for NewRelic timestamp
4. Email notification when other nodes are not available and script is trying to restart tomcat (maybe after 3 attempt)
5. NewRelic Insights push JSON log using REST API every 60 seconds (or at a configured interval) so it doesn't push everytime a new line is found


## Guide to Run a Script

If you want to run this script on your machine, follow below steps:

1. Clone repo by running ``git clone https://devc3100@bitbucket.org/devc3100/gc-log-parser.git`` command on your terminal
2. Go to gc-log-parser directoy
3. Verify you have python 2.7.16 or higher version installed on machine, run ``python --version`` command on terminal to verify
4. Open the ``script.py`` and verify the ``current_datetime`` variable, if you are using same log file in repo leave it as is
5. If you are using live log file comment out ``line no. 31`` and uncomment ``line no. 32`` to capture current timestamp
6. In case you're using love logs update `log_file` variable on ``line no. 23`` to capture live logs or your custom log file
7. After everything is setup, run ``python script.py`` in your terminal
8. Scirpt will traverse through ``logs\gc.log`` file and wait for new log line (trailing line) to appear

### Rotate Log File 

To test if script is working during file log rotation, please follow below steps:

1) Rotate current log file with new empty log file

```
mv gc.log gc.log-20190703 && touch gc.log
```

2) Push a new log line to gc.log file, run one command at a time in new terminal window and check the script window, it shall read and process the new line

```
echo "2017-03-01T16:08:00.551-0500: 6689.759: [Full GC (Allocation Failure)  10198M->10100M(10200M), 5.4572036 secs]" >> gc.log
echo "2017-03-01T17:08:26.441-0500: 6699.114: [GC pause (G1 Evacuation Pause) (young), 0.0429834 secs]" >> gc.log
echo "2017-03-01T15:17:04.169-0500: 16.841: [GC concurrent-mark-end, 0.0069650 secs]" >> gc.log
echo "2017-03-01T15:43:33.656-0500: 1606.329: [GC concurrent-cleanup-end, 0.0000530 secs]" >> gc.log
```

## NewRelic Insights API JSON Request

To push custom events to NewRelic insights, you need to follow below url format:

```
gzip -c example_events.json | curl --data-binary @- -X POST -H "Content-Type: application/json" -H "X-Insert-Key: YOUR_KEY_HERE" -H "Content-Encoding: gzip" https://insights-collector.newrelic.com/v1/accounts/1234567/events
```

Custom JSON created in script that needs to be sent to NewRelic:

``
[{'Duration': 5.4572036, 'Timestamp': '2017-03-01 16:08:00.551000', 'Event': 'Full GC ', 'Stacktrace': '2017-03-01T16:08:00.551-0500: 6689.759: [Full GC (Allocation Failure)  10198M->10100M(10200M), 5.4572036 secs]\n'}, {'Duration': 1.1669987, 'Timestamp': '2017-03-01 17:08:13.478000', 'Event': 'GC pause ', 'Stacktrace': '2017-03-01T17:08:13.478-0500: 6686.150: [GC pause (G1 Evacuation Pause) (mixed) (to-space exhausted), 1.1669987 secs]\n'}, {'Duration': 0.0429834, 'Timestamp': '2017-03-01 17:08:26.441000', 'Event': 'GC pause ', 'Stacktrace': '2017-03-01T17:08:26.441-0500: 6699.114: [GC pause (G1 Evacuation Pause) (young), 0.0429834 secs]\n'}, {'Duration': 0.2340986, 'Timestamp': '2017-03-01 15:43:43.759000', 'Event': 'GC pause ', 'Stacktrace': '2017-03-01T15:43:43.759-0500: 1616.432: [GC pause (G1 Evacuation Pause) (mixed), 0.2340986 secs]\n'}]
``

